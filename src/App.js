import React from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import {Routing} from "./Routing";
//import {Menu} from "./components/Menu";

function App() {
  return (
    <div>
      <Router>
        <Routing/>
      </Router>
    </div>
  );
}

export default App;
