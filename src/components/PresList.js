import React, {useEffect, useState} from 'react'
import API from '../services/api'
import { Collapse, Divider, Button, Row, Col, Descriptions, notification } from 'antd';
import DateFormat from 'dateformat'

export const PresList = () => {
    const [presentations, setPresentations] = useState([])
    const [onlyPresentation] = useState(false)

    const header = API.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token');

    useEffect(() => {
        async function fetchData(){
            const promRoom = API.get(`/rooms`)
            const promPres = API.get(`/presentations`)
            const promSession = API.get(`/sessions`)

            await Promise.all([promPres,promSession,promRoom])
            .then(function(values){
                //console.log(values)
                const presList = values[0].data.map(pres => {
                    //console.log(values[1].data)
                    
                    const sessionFinded = values[1].data[pres.session]
                    const roomFinded = values[2].data[sessionFinded.localization]

                    return {
                        id: pres.id,
                        title: pres.title,
                        authors: pres.authors,
                        date: pres.date,
                        filename: pres.filename,
                        session: sessionFinded.name,
                        room: roomFinded.name
                    }
                })
                //console.log(presList)
                setPresentations(presList)
            })
            .catch((error) => {
                notification['error']({
                    message: 'Something went wrong',
                    description: 'Something went wrong with GET request.'
                })
            });
        }
        fetchData()
    }, [onlyPresentation])

    const handleAdd = (event) => {
        API.post(`/reminders`, {
            presentationId: event,
            notes: 'Insert your notes...'
        }, header)
            .then(info => {
                console.log(info)
                notification['success']({
                    message: 'Added to reminders!',
                    description: 'This presentation was successfully added to your reminders.'
                })
            })
            .catch((error) => {
                console.error(error)
                notification['error']({
                    message: 'Cannot add to reminders!',
                    description: 'Something went wrong (check if you\'re logged in).'
                }) 
            });
    }

    return (
        <>
            <Collapse accordion>
                {presentations.map(presentation => 
                <Collapse.Panel header={presentation.title} key={presentation.id}>

                    <Descriptions bordered size="small">
                        <Descriptions.Item label="Title">{presentation.filename? (<a href={"https://ie2020.kisim.eu.org/api/abstracts/" + presentation.filename}>{presentation.title}</a>):presentation.title}</Descriptions.Item>
                        <Descriptions.Item label="Date">{DateFormat(presentation.date, "dddd, mmmm dS, yyyy, h:MM:ss TT")}</Descriptions.Item>
                        <Descriptions.Item label="Authors">{presentation.authors?.map(a => <li key={a}>{a}</li>)}</Descriptions.Item>
                        <Descriptions.Item label="Room">{presentation.room}</Descriptions.Item>
                        <Descriptions.Item label="Session">{presentation.session}</Descriptions.Item>
                    </Descriptions>
                    <Divider/>
                    <Row>
                        <Col span={1}><Button onClick={() => handleAdd(presentation.id)}>REMIND ME</Button></Col>
                    </Row>     
                </Collapse.Panel>
                )}
            </Collapse>
        </>
    )
}