import React, { useEffect, useState} from 'react'
import { Link, withRouter } from 'react-router-dom';
import 'antd/dist/antd.css';
import { Menu, Button } from 'antd';
import {useHistory} from "react-router-dom";

export const MyMenu = withRouter(props => {
    const { location } = props
    const [isLoggedIn, setIsLoggedIn] = useState([])
    const history = useHistory();

    useEffect(()=>{
        if(localStorage.getItem("token") === null){
            setIsLoggedIn(false)
        }
        else{
            setIsLoggedIn(true)
        }
    },[])

    
    function handleLoginButton(){
        if(isLoggedIn === true){
            if(location.pathname === "/reminders") history.push("/presentations")
            localStorage.clear()
            setIsLoggedIn(false)
        }
        else{
            history.push("/")
        }
    }

    return (
        
        <Menu style={{backgroundColor: '#F5F5F5'}} mode="horizontal" selectedKeys={[location.pathname]}>
        
          <Menu.Item key="/presentations">
            <Link to="/presentations">Presentations</Link>
          </Menu.Item>
          <Menu.Item key="/reminders">
            <Link to="/reminders">Reminders</Link>
          </Menu.Item>
          
          <Menu.Item style={{float: 'right'}} key="/authorization">
          <Button type={isLoggedIn? "dashed" : "primary"} onClick={() => handleLoginButton()}>{isLoggedIn? 'Logout' : 'Login'}</Button>
          </Menu.Item>
          
        </Menu>
        
  )
})