import React, {useEffect, useState} from 'react'
import API from '../services/api'
import {useHistory} from "react-router-dom";
import { Collapse, Divider, Button, Switch, Row, Col, Descriptions, notification } from 'antd';
import DateFormat from 'dateformat';

export const RemindList = () => {
    const [reminders, setReminders] = useState([])
    const [onlyReminder] = useState(false)
    const history = useHistory();

    const header = API.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token');

    useEffect(() => {
        if(localStorage.getItem('token') === null){
            history.push("/presentations")
        }
        async function fetchData(){
            const promRemind = API.get(`/reminders`, {}, header)
            const promPres = API.get(`/presentations`)

            await Promise.all([promRemind,promPres])
            .then(function(values){
                //console.log(values)
                const remindList = values[0].data.map(rem => {
                    const presFinded = values[1].data.filter(function(pres){
                        return pres.id === rem.presentationId
                    })[0]
                    //console.log(rem.notes)
                    return {
                        title: presFinded.title,
                        date: presFinded.date,
                        remId: rem.id,
                        presentationId: rem.presentationId,
                        enabled: rem.enabled
                    }
                })
                //console.log(remindList)
                setReminders(remindList)
            })
            .catch((error) => {
                localStorage.clear()
                history.push("/presentations")
            });  
        }
        fetchData()
    }, [onlyReminder,header,history])

    const handleDelete = (id) => {
        API.delete(`/reminders/${id}`, {}, header)
            .then((response) => {
                const newRems = reminders.filter(function(rem){
                    return rem.remId !== id
                })
                setReminders(newRems)
                notification['success']({
                    message: 'Reminder removed!',
                    description: 'This presentation was successfully removed from your reminders.'
                })
            })
            .catch((error) => {
                notification['error']({
                    message: 'Reminder not removed!',
                    description: 'Something went wrong (check if you\'re logged in).'
                })
            });
    }

    function onChange(id, presId, checked) {
        //console.log(presId)
        API.put(`/reminders/${id}`, {
            presentationId: presId,
            enabled: checked,  
        }, header)
            .then((response) => {
                const newRems = reminders.filter(function(rem){
                    if(rem.remId === id){
                        rem.enabled = checked
                    }
                    return reminders
                })
                setReminders(newRems)
            })
            .catch((error) => {
                notification['error']({
                    message: 'Reminder can\'t be changed!',
                    description: 'Something went wrong (check if you\'re logged in).'
                })
            });
      }
        
    return (  

            <Collapse accordion>
                {reminders.map(reminder => 
                <Collapse.Panel style={{backgroundColor: reminder.enabled? '':'#CD5C5C'}} header={reminder.title} key={reminder.remId}>

                    <Descriptions bordered size="small">
                        <Descriptions.Item label="Date">{DateFormat(reminder.date, "dddd, mmmm dS, yyyy, h:MM:ss TT")}</Descriptions.Item>
                    </Descriptions>
                    <Divider/>
                    <Row>
                        <Col><Button danger onClick={() => handleDelete(reminder.remId)}>DELETE</Button></Col>
                        <Col span={1} offset={20}>
                        <Switch checkedChildren="enabled" unCheckedChildren="disabled" defaultChecked={reminder.enabled} onChange={checked => onChange(reminder.remId, reminder.presentationId, checked)} />
                        </Col>
                    </Row>     
                </Collapse.Panel>
                )}
            </Collapse>
            
    )
}