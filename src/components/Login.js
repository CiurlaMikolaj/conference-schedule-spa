import React from 'react'
import {useHistory} from 'react-router-dom'
import {Button, Card, Form, Input, notification} from "antd";
import API from '../services/api'

export const Login = () => {
    const [form] = Form.useForm();
    const history = useHistory();

    const handleOnFinish = (values) => {
        API.post('/auth',{},{
            auth:{
                username: values.username,
                password: values.password
            }
        })
            .then(response => {
                localStorage.setItem("token", response.data.token)
                history.push('/presentations')
            })
            .catch(errInfo => {
                notification['error']({
                    message: 'Błąd logowania!',
                    description: 'Podałeś nieprawidłowe dane w formularzu'
                })
            })
    }

    return (
        <>
            <Card title='Login form'>
                <Form form={form} onFinish={handleOnFinish}>
                    <Form.Item name='username'>
                        <Input  placeholder='Username'/>
                    </Form.Item>
                    <Form.Item name='password'>
                        <Input  type='password' placeholder='Password'/>
                    </Form.Item>
                    <Form.Item>
                        <Button type='primary' htmlType='submit'>Login</Button>
                    </Form.Item>
                </Form>
            </Card>
        </>
    )
}