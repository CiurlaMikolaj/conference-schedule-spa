import React from 'react'
import {
    Switch,
    Route
} from "react-router-dom";
import {Login} from "./components/Login";
import {PresList} from "./components/PresList";
import {RemindList} from "./components/RemindList";
import {MyMenu} from "./components/Menu";
import {Page404} from "./components/404";

export const Routing = () => {
    return (
        <>
            <Switch>
                <Route exact path="/">
                    <Login/>
                </Route>
                <Route exact path="/presentations">
                    <MyMenu/>
                    
                    <PresList/>
                </Route>
                <Route exact path="/reminders">
                    <MyMenu/>
                    
                    <RemindList/>
                </Route>
                <Route component={Page404}/>
            </Switch>
        </>
    )
}